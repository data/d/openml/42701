# OpenML dataset: Methane

https://www.openml.org/d/42701

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Marek Sikora, Lukasz Wrobel  
**Source**: coal mine in Poland - March 2, 2014 - June 16, 2014  
**Please cite**: Slezak, D., Grzegorowski, M., Janusz, A., Kozielski, M., Nguyen, S. H., Sikora, M., Stawicki, S. &amp; Wrobel, L. (2018). A framework for learning and embedding multi-sensor forecasting models into a decision support system: A case study of methane concentration in coal mines. Information Sciences, 451, 112-133.  

Coal mining requires working in hazardous conditions. Miners in an underground coal mine can face several threats, such as, e.g. methane explosions or rock-burst. To provide protection for people working underground, systems for active monitoring of a production processes are typically used. One of their fundamental applications is screening dangerous gas concentrations (methane in particular) in order to prevent spontaneous explosions. Therefore, for that purpose the ability to predict dangerous concentrations of gases in the nearest future can be even more important than monitoring the current sensor readings.

The data set contains raw data collected at an underground coal mine. It consists of a data stamp and measurements collected each second. The considered task related to this data set is to construct a model capable of predicting dangerous concentrations of methane at longwalls of a coal mine in a chosen time horizon,
Therefore, in case of classification task the model has to predict weather the methane concentration for three methane meters: MM263, MM264 and MM256 exceeds the chosen threshold (e.g. 1.0) within the chosen period of time (e.g. three to six minutes). In case of regression task it is required to predict the value of methane concentration for the selected methane meters.

### Attribute Information:
AN311 - anemometer (distant) [m/s]

* sensor type: anemometer -5-5
* kind: alarming

AN422 - anemometer [m/s]

* sensor type: anemometer -5-5
* kind: switching off

AN423 - anemometer [m/s]

* sensor type: anemometer -5-5
* kind: switching off

TP1721 - temperature [C]

* sensor type: temperature THP (three-component sensor THP2/93)
* kind: registering

RH1722 - humidity [%RH]

* sensor type: humidity THP (three-component sensor THP2/93)
* kind: registering

BA1723 - barometer [hPa]

* sensor type: barometer THP (three-component sensor THP2/93)
* kind: registering

TP1711 - temperature [C]

* sensor type: temperature THP (three-component sensor THP2/94)
* kind: registering

RH1712 - humidity [%RH]

* sensor type: humidity THP (three-component sensor THP2/94)
* kind: registering

BA1713 - barometer [hPa]

* sensor type: barometer THP (three-component sensor THP2/94)
* kind: registering

MM252 &ndash; methane meter (distant) [%CH4]

* sensor type: methane meter MM-2PWk
* kind: switching off
* value of threshold A (alarm): 2.0%
* value of threshold W (warning): 1.5%

MM261 &ndash; methane meter [%CH4]

* sensor type: methane meter MM-2PWk
* kind: switching off
* value of threshold A: 1.5%
* value of threshold W: 1.0%

MM262 - methane meter [%CH4]

* sensor type: methane meter MM-2PWk
* kind: switching off
* value of threshold A: 1.0%
* value of threshold W: 0.6%

MM263 - methane meter [%CH4] - !target sensor!

* sensor type: methane meter MM-2PWk
* kind: switching off
* value of threshold A: 1.5%
* value of threshold W: 1.0%

MM264 - methane meter [%CH4] - !target sensor!

* sensor type: methane meter MM-2PWk
* kind: switching off
* value of threshold A: 1.5%
* value of threshold W: 1.0%

MM256 - methane meter [%CH4] - !target sensor!

* sensor type: methane meter MM-2PWk
* kind: switching off
* value of threshold A: 1.5%
* value of threshold W: 1.0%

MM211 - methane meter [%CH4]

* sensor type: methane meter MM-2PWk
* kind: switching off
* value of threshold A: 2.0%
* value of threshold W: 1.5%

CM861 &ndash; high concentration methane meter [%CH4]

* sensor type: methane meter (0&hellip;100)
* kind: registering

CR863 &ndash; sensor for pressure difference on the methane drainage flange [Pa]

* sensor type: pressure difference (0..250)
* kind: registering

CR863 - sensor for pressure difference on the methane drainage flange [Pa]

* sensor type: pressure difference (0..250)
* kind: registering

P_864 &ndash; pressure inside the methane drainage pipeline [kPa]

* sensor type: pressure (0..110)
* kind: registering

TC862 &ndash; temperature inside the pipeline [C]

* sensor type: temperature (10..40)
* kind: registering

WM868 &ndash; methane delivery calculated according to CM, CR, P, TC [m3/mi]

* sensor type: methane delivery (0..50)
* kind: registering

AMP1_IR - current of the left cutting head of the cutter loader [A]

AMP2_IR - current of the right cutting head of the cutter loader [A]

DMP3_IR - current of the left haulage in the cutter loader [A]

DMP4_IR - current of the right haulage in the cutter loader [A]

AMP5_IR &ndash; current of the hydraulic pump engine in the cutter loader [A]

F_SIDE - driving direction, 1=left, {0, 0.5}=right

V - cutter loader speed [Hz] (Vmin=3Hz, Vmax=100Hz. Herz values are then transformed into m/min - 100Hz equal to about 20 m/min)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42701) of an [OpenML dataset](https://www.openml.org/d/42701). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42701/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42701/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42701/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

